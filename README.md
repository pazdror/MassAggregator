# Mass aggregator
This is a python code for mass aggregation of scientific data, as described at:
"Robust mass aggregation-based bedload flux prediction in flash flood conditions using an acoustic sensor" by 
Eran Halfi, Dror Paz, Kyle Stark, Ian Reid, Michael Dorman, Jonathan B. Laronne

## Usage:
Input must be excel file or dataframe with datetime index, and with the columns:
 - level - water level in cm. 
 - weight - cumulative sample weight. 
 - pulses - amount of pulses per record. 

## requierements:
 - python >= 2.7 (Should also work on 3 and above, not tested)
 - pandas