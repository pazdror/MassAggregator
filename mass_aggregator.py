__author__ = 'Dror Paz, BGU'
CreatedOn = '2018/03/21'

import pandas as pd


class MassAggregator:
    '''
    For more information on mass aggregation, please see:
    "Robust mass aggregation-based bedload flux prediction in flash flood conditions using an acoustic sensor" by
    Eran Halfi, Dror Paz, Kyle Stark, Ian Reid, Michael Dorman, Jonathan B. Laronne

    Usage:
    Input must be excel file or dataframe with datetime index, and with the columns:
    level - water level in cm.
    weight - cumulative sample weight.
    pulses - amount of pulses per record.
    '''

    @classmethod
    def aggregate_from_excel(cls, filename, mass_threshold=4, trap_width=0.11, level_col='level', weight_col='weight',
                             pulses_col='pulses'):
        in_df = pd.read_excel(filename, index_col=0, parse_dates=True)
        agg = cls(in_df, mass_threshold=mass_threshold, trap_width=trap_width, level_col=level_col, 
                  weight_col=weight_col, pulses_col=pulses_col)
        agg = agg.aggregate(mass=mass_threshold)
        return agg

    def __init__(self, in_df, mass_threshold, trap_width=0.11, level_col='level', weight_col='weight',
                 pulses_col='pulses'):
        assert level_col in df.columns
        assert weight_col in df.columns
        assert pulses_col in df.columns
        self.df = in_df
        self.mass_threshold = mass_threshold
        self.level_col = level_col
        self.weight_col = weight_col
        self.pulses_col = pulses_col
        self.trap_width = trap_width

    def aggregate(self, mass):
        '''
        A script that applies mass-based aggregation on time-based database.
        :param mass: mass threshold for aggregation
        :return: mass-aggregated dataframe
        '''

        def _get_seconds(time_delta):
            return time_delta.total_seconds()

        weight_diff_col = 'weight_diff'

        # Created dataframe to be aggragated
        agg_df = df.loc[:, [self.level_col, self.pulses_col, self.weight_col]]

        agg_df[weight_diff_col] = df[self.weight_col].diff()
        agg_df.loc[agg_df.index[0], weight_diff_col] = 0

        agg_df['time_diff'] = df.index.to_series().diff().apply(_get_seconds)

        # setting new mass index and cumulative weight
        agg_df['weight_cumm'] = 0.
        agg_df['mass_index'] = 0
        for i in range(1, len(df)):
            if agg_df.weight_cumm.iloc[i - 1] >= mass:
                agg_df.weight_cumm.iloc[i] = agg_df[weight_diff_col].iloc[i]
                agg_df.mass_index.iloc[i] = agg_df.mass_index.iloc[i - 1] + 1
            else:
                agg_df.weight_cumm.iloc[i] = agg_df.weight_cumm.iloc[i - 1] + agg_df[weight_diff_col][i]
                agg_df.mass_index.iloc[i] = agg_df.mass_index.iloc[i - 1]
        out_df = agg_df.groupby(agg_df.mass_index).apply(self._time_df_to_mass_df)
        return out_df

    def _time_df_to_mass_df(self, in_gdf):
        '''
        :param in_gdf: date frame with columns: time_diff, level, pulses, weight_cumm
        :return:
        '''
        out_s = pd.Series()
        out_s['Time'] = in_gdf.index.max()
        out_s['time_length'] = in_gdf.time_diff.sum()
        out_s['level'] = in_gdf.level.max()
        out_s['pulses'] = in_gdf.pulses.mean()
        out_s['pulses_sum'] = in_gdf.pulses.sum()
        out_s['pulses_rate'] = in_gdf.pulses.sum() / out_s['time_length']
        out_s['weight_flux'] = in_gdf.weight_cumm.max() / self.trap_width / out_s['time_length']
        return out_s


if __name__ == '__main__':
    in_file = r'C:\Users\USER\Documents\Article\in_xlsx\2018.01.19.xlsx'
    df = MassAggregator.aggregate_from_excel(in_file, 4, 0.11, 'L7', 'weight', 'pulses')
    print(df)
